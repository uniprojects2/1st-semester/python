# Επιστήμη Υπολογιστών - Python

A very simple quiz game using Python.
The game uses three arrays to store the the questions, the possible answers and the correct answers.
The player(s) answer each question in order. They are also given a number of hints to use throughout the game.
When the players have finished answering the questions, the program calculates and shows a leaderboard with the highest scores.
